# frozen_string_literal: true

module Ticker
  class Configuration < WidgetInstanceConfiguration
      attribute :amount, :integer, default: 5
      attribute :show_timestamp, :boolean, default: false
      attribute :show_feed_icon, :boolean, default: true
      attribute :rotation_interval, :integer, default: 10

      validates :show_timestamp, :show_feed_icon, boolean: true
      validates :amount,
                numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 10 }
      validates :rotation_interval,
                numericality: { greater_than_or_equal_to: 5, less_than_or_equal_to: 60 }
  end
end
