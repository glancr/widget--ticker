module Ticker
  class Engine < ::Rails::Engine
    isolate_namespace Ticker
    config.generators.api_only = true
  end
end
