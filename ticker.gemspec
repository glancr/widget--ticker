require 'json'
$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ticker/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ticker"
  s.version     = Ticker::VERSION
  s.authors     = ["Tobias Grasse"]
  s.email       = ["tg@glancr.de"]
  s.homepage    = "https://glancr.de/modules/ticker"
  s.summary     = "mirr.OS widget that displays headlines from news sources."
  s.description = "Shows the headlines of configured newsfeeds."
  s.license     = "MIT"
  s.metadata    = { 'json' =>
                  {
                    type: 'widgets',
                    title: {
                      enGb: 'Ticker',
                      deDe: 'Ticker'
                    },
                    description: {
                      enGb: s.description,
                      deDe: 'Zeigt die Schlagzeilen der konfigurierten Newsfeeds.'
                    },
                    sizes: [
                      {
                        w: 6,
                        h: 4
                      }
                    ],
                    languages: [:enGb, :deDe],
                    group: 'newsfeed' # see https://gitlab.com/glancr/mirros_one/wikis/home for available groups
                  }.to_json
                }

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_development_dependency 'rails'
  s.add_development_dependency 'rubocop', '~> 0.81'
  s.add_development_dependency 'rubocop-rails'
end
